
jQuery UI filter
----------------

The jQueryUI filter converts static HTML to a JQuery UI accordion or tabs widget.

VERSIONS
--------

### Drupal 6 & 7

The Drupal 6 & 7 version of this module is minimally maintained, any reasonable
patches will be applied.

### Drupal 8

The Drupal 8 version of this module is a complete rewrite that moves the 
conversion of HTML header tags to a JQuery UI accordion or tabs widgets into 
a [JavaScript library][http://jsfiddle.net/jrockowitz/raLvc6hj/] 
that uses data attributes to trigger the conversion.  

The configuration settings have been simplifed to support future versions of 
jQuery UI.

Please note, Drupal 8 core now supports jQuery UI dialog handling so this 
functionality has been remove from the module.


